<?php

require_once('Figura.php');


class Circulo implements Figura {

	private $radio;


	public function __construct() {
		$this->radio = 3;
	}

	public function imprimir() {
		echo $this->tipo() . ": \n"
			. ' Superficie = ' . $this->superficie()
			. ', Base = ' . $this->base()
			. ', Altura = ' . $this->altura()
			. ', Diametro = ' . $this->diametro() . " \n<br/>";
	}
	

	public function superficie(){
		return $this->radio * 3.14;
	}

	public function base(){
		return "N/A";
	}

	public function altura(){
		return "N/A";
	}

	public function diametro(){
		return $this->radio * 2;
	}

	public function tipo(){
		return 'Círculo';
	}

}