<?php

require_once('Figura.php');


class Triangulo implements Figura {

	private $base;
	private $altura;


	public function __construct() {
		$this->base = 6;
		$this->altura = 4;
	}

	public function imprimir() {
		echo $this->tipo() . ": \n"
			. ' Superficie = ' . $this->superficie()
			. ', Base = ' . $this->base()
			. ', Altura = ' . $this->altura()
			. ', Diametro = ' . $this->diametro() . " \n<br/>";
	}
	

	public function superficie(){
		return 0.5 * $this->base * $this->altura;
	}

	public function base(){
		return $this->base;
	}

	public function altura(){
		return $this->altura;
	}

	public function diametro(){
		return "N/A";
	}

	public function tipo(){
		return 'Triángulo';
	}

}