<?php

interface Figura {
	
	public function superficie();

	public function base();

	public function altura();

	public function diametro();

	public function tipo();

}
