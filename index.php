<?php

require_once('FiguraGeometrica.php');


$f1 = FiguraGeometrica::generar(CUADRADO);
$f2 = FiguraGeometrica::generar(TRIANGULO);
$f3 = FiguraGeometrica::generar(CIRCULO);


$f1->imprimir();
$f2->imprimir();
$f3->imprimir();
