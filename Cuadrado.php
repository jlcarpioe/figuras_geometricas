<?php

require_once('Figura.php');


class Cuadrado implements Figura {

	private $base;


	public function __construct() {
		$this->base = 2;
	}

	public function imprimir() {
		echo $this->tipo() . ": \n"
			. ' Superficie = ' . $this->superficie()
			. ', Base = ' . $this->base()
			. ', Altura = ' . $this->altura()
			. ', Diametro = ' . $this->diametro() . "\n<br/>";
	}
	

	public function superficie(){
		return $this->base**2;
	}

	public function base(){
		return $this->base;
	}

	public function altura(){
		return $this->base();
	}

	public function diametro(){
		return "N/A";
	}

	public function tipo(){
		return 'Cuadrado';
	}

}