<?php

require_once('Cuadrado.php');
require_once('Circulo.php');
require_once('Triangulo.php');

const CUADRADO = 0;
const TRIANGULO = 1;
const CIRCULO = 2;

class FiguraGeometrica {
	
	public static function generar($tipo) {
		switch ($tipo) {
			case CUADRADO:
				return new Cuadrado();

			case CIRCULO:
				return new Circulo();

			case TRIANGULO:
				return new Triangulo();
			
			default:
				return '';
				break;
		}
	}

}